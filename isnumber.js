function isDigit(c) {
    return  c === '0' || c === '1' || c === '2' || c === '3' ||
            c === '4' || c === '5' || c === '6' || c === '7' ||
            c === '8' || c === '9';
}

function isNumber(str, sep) {
    if(sep === undefined) {
        sep = ",";
    }
    var result = true;
    var usesComma = false;
    var allZero = true;
    var counter = -1;
    for(var i = str.length - 1; i > -1; i--) {
        var c = str[i];
        counter += 1;
        if(counter === 3) {
            if(c !== sep && usesComma) {
                //console.log("Uses `%s` but no `%s`!", sep, sep);
                result = false;
                break;
            } else if(c === sep) {
                if(i === (str.length - 4)) {
                    // If it's the first ever comma
                    usesComma = true;
                } else if(!usesComma) {
                    //console.log("Not consistent with `%s`s!", sep);
                    result = false;
                    break;
                }
                counter = -1;
            } else {
                // Need this because if there isn't a comma
                // the counter will be off by 1 (because it expected a comma)
                counter = 0;
            }
        } else if(!isDigit(c)) {
            //console.log("Expected a digit!");
            result = false;
            break;
        } else if(c !== '0') {
            allZero = false;
        }
    }
    // Some extras.
    if(allZero) {
        //console.log("Your number is all zeros!");
    } else if(str[0] === '0') {
        //console.log("You have a leading zero!");
    }
    return result;
}

function assert(cond, msg) {
    if(msg === undefined) {
        msg = "";
    }
    if(!cond) {
        console.log("Assertion Failed! " + msg);
    }
}

function tests() {
    assert(isNumber("100"), "100");
    assert(isNumber("100,000"), "100,000");
    assert(isNumber("1,000,000"), "1,000,000");
    assert(isNumber("100000"), "100000");
    assert(isNumber("1000000"), "1000000");
    	
    assert(!isNumber("1,00"), "1,00");
    assert(!isNumber("10,00,000"), "10,00,000");
    assert(!isNumber(",100000"), ",100000");
    assert(!isNumber(",1,00,010"), ",1,00,010");
    assert(!isNumber(",,,,,,"), ",,,,,,");

	// For users that use . instead of .
    assert(isNumber("100.000", "."), "100.000");
}
